```
simple brute force algorithm for md5($salt.md5($pass))
admin:admin -> c0e024d9200b5705bc4804722636378a
```

```
$> go run gn.go

root password 9e638176abac531e0bdc95d73e1f06e5
```

```
$> go run pw.go --start=p --stop=p --length=8  --username=root --dictionary=abcdefghijklmnopqrstuvwxyz --hash=9e638176abac531e0bdc95d73e1f06e5

Username: root
Length: 8
Start: p
Stop: p
Dictionary: abcdefghijklmnopqrstuvwxyz
Hash: 9e638176abac531e0bdc95d73e1f06e5
Combinations: ~8353082582

[p a a a a a a a] -> 0% 0m/0m
[p a b r t x u u] -> 0% 1m/347m
[p a f b h t k i] -> 1% 2m/345m
[p a g t b r f c] -> 1% 3m/345m
[p a i k v o z w] -> 1% 4m/344m
[p a k y m l s a] -> 2% 5m/345m
[p a l u j k p k] -> 2% 6m/346m
[p a p d x g e y] -> 2% 7m/346m
[p a q v r d z s] -> 2% 8m/345m
[p a s n l b u m] -> 3% 9m/345m

[SUCCESS] Finished in 9 min

(¯`·._.·(¯`·._.·  p  a  s  s  w  o  r  d  ·._.·´¯)·._.·´¯)
```

## To potential contributors

I wrote this code for very short time, but there are some things I wish to do and hope somebody continue this project...  
  
— fix mess with passed and global variables  
— now you can split a big task by running multiple instances of app for every starting symbol of password (`--start=a --start=a`), but for big passwords need to split into even smaller parts  
— add example how to split a big task to run 100+ instances in docker swarm, if password is found than send notification to telegram  