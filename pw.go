package main

import (
	"crypto/md5"
	"encoding/hex"
	"flag"
	"fmt"
	"math"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"
)

var start string
var stop string
var pwLength int
var hash string
var dictionary string
var username string

func init() { // command line flags
	flag.StringVar(&start, "start", "g", "")
	flag.StringVar(&stop, "stop", "g", "")
	flag.IntVar(&pwLength, "length", 6, "")
	flag.StringVar(&hash, "hash", "1c388ee419526461e0bfc90d96d8c48a", "") // demo pw: "go.pwd"
	flag.StringVar(&dictionary, "dictionary", "abcdefghijklmnopqrstuvwxyz.ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890", "")
	flag.StringVar(&username, "username", "admin", "")
}

var startTime = time.Now()
var elapsed string
var consumers = 16
var bufferedChannel = 10
var procs = 16
var startMask []int
var stopMask []int

func nextPassword(n int, dic string, startM []int, stopM []int) func() string {
	r := []rune(dic)
	pw := make([]rune, n)
	var mask string
	var count int

	var combForOne float64
	// There is a small error, numbers don't match
	// Length: 5
	// Start: 1
	// Stop: 0
	// Dictionary: 1234567890
	// Combinations: ~111100 --> In fact: 99999
	for i := 1; i <= pwLength-1; i++ {
		combForOne += math.Pow(float64(len(dic)), float64(i))
	}

	startPos := strings.Index(dic, start)
	stopPos := strings.Index(dic, stop) + 1
	combinations := (stopPos - startPos) * int(combForOne)

	fmt.Printf("\nUsername: %s\n", username)
	fmt.Printf("Length: %d\n", pwLength)
	fmt.Printf("Start: %s\n", start)
	fmt.Printf("Stop: %s\n", stop)
	fmt.Printf("Dictionary: %s\n", dictionary)
	fmt.Printf("Hash: %s\n", hash)
	fmt.Printf("Combinations: ~%d\n\n", combinations)

	return func() string {
		pw := pw[:len(startM)]
		for i, xi := range startM {
			pw[i] = r[xi]
		}

		if count%10000000 == 0 { // print each 10_000_000 th
			elapsedTime := float64(combinations) / (float64(count) / time.Since(startTime).Minutes())
			if fmt.Sprint(elapsedTime) == "+Inf" || fmt.Sprint(elapsedTime) == "NaN" {
				elapsedTime = 0.0
			}
			fmt.Printf("%c -> %.0f%% %.0fm/%.0fm\n", pw, percentOf(count, combinations), time.Since(startTime).Minutes(), elapsedTime)
		}

		mask = fmt.Sprintf("%d", startM)
		if mask == fmt.Sprintf("%d", stopM) {
			fmt.Printf("\n[NOT FOUND] Finished in %.0fm. Total: %d, %.0f/s\n\n", time.Since(startTime).Minutes(), count, float64(count)/time.Since(startTime).Seconds())
			os.Exit(0)
		}

		for i := len(startM) - 1; i >= 0; i-- {
			startM[i]++
			if startM[i] < len(r) {
				break
			}
			startM[i] = 0
			if i <= 0 {
				startM = startM[0:0]
				break
			}
		}
		count++
		return string(pw)
	}
}

var wg sync.WaitGroup

func produce(ch chan<- []byte, startM []int, stopM []int) {
	// generate all passwords:
	np := nextPassword(pwLength, dictionary, startM, stopM)

	for {
		pass := np()
		if len(pass) == 0 {
			break
		}
		ch <- []byte(pass) // send it for processing
	}
	close(ch)
}

func consume(ch <-chan []byte) {
	defer wg.Done()
	for pass := range ch {
		// Hash, check
		genHash := getMD5Hash(fmt.Sprintf("%s%s", username, getMD5Hash(string(pass[:]))))
		if hash == genHash {
			// Password found!
			fmt.Printf("\n[SUCCESS] Finished in %.0f min\n\n", time.Since(startTime).Minutes())

			var result string
			for _, s := range pass {
				x := fmt.Sprintf(" %c ", s)
				result += x
			}

			fmt.Printf("(¯`·._.·(¯`·._.· %s ·._.·´¯)·._.·´¯)\n", result)
			os.Exit(0)
		}
	}
}

func main() {
	flag.Parse()
	startMask = genStartMask(start)
	stopMask = genStopMask(stop)

	runtime.GOMAXPROCS(procs)
	ch := make(chan []byte, bufferedChannel) // Buffered channel

	//Start consumers:
	for i := 0; i < consumers; i++ {
		wg.Add(1)
		go consume(ch)
	}

	// Start producing: we can run this in the main goroutine
	produce(ch, startMask, stopMask)
	wg.Wait() // Wait all consumers to finish processing passwords

}

func getMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

func genStartMask(symbol string) []int {
	slice := make([]int, pwLength)
	slice[0] = sliceIndex(len(dictionary), func(i int) bool { return fmt.Sprintf("%c", dictionary[i]) == symbol })
	return slice
}

func genStopMask(symbol string) []int {
	slice := make([]int, pwLength)
	for i := 1; i < pwLength; i++ {
		slice[i] = len(dictionary) - 1
	}
	slice[0] = sliceIndex(len(dictionary), func(i int) bool { return fmt.Sprintf("%c", dictionary[i]) == symbol })
	return slice
}

func sliceIndex(limit int, predicate func(i int) bool) int {
	for i := 0; i < limit; i++ {
		if predicate(i) {
			return i
		}
	}
	return -1
}

func percentOf(current int, all int) float64 {
	percent := (float64(current) * float64(100)) / float64(all)
	return percent
}
