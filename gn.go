package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
)

func getMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}

var username = "root"
var password = "password"

func main() {
	genhash := getMD5Hash(fmt.Sprintf("%s%s", username, getMD5Hash(password)))
	fmt.Println(username, password, genhash)

}
